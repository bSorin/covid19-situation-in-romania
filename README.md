# COVID-19 Data Analysis for Romania

## Project Overview

This project analyzes the COVID-19 data for Romania using three datasets: confirmed cases, deaths, and recoveries. The data is processed, filtered, and visualized to provide insights into the progression of the pandemic over time, focusing on the year 2020 and early 2021.

## Requirements

- Python 3.x
- pandas
- matplotlib

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/bSorin/covid19-situation-in-romania.git 
   cd covid19-situation-in-romania
   ```

2. Install the required packages:

   ```bash
   pip install pandas matplotlib
   ```

3. Ensure the datasets (`COVID19_confirmed.csv`, `COVID19_deaths.csv`, and `COVID19_recovered.csv`) are placed in the same directory as the script.

## Data Preparation

The datasets are loaded and processed to focus on Romania's data. Unnecessary columns are removed, and the data is filtered by year.

## Code Description

### Class Definition

The `Covid` class is defined to handle data processing and visualization:

```python
import pandas as pd
import matplotlib.pyplot as plt

class Covid:
    def __init__(self, df, x, y):
        self.df = df
        self.x = x
        self.y = y

    def delete_extra_rows(self):
        self.df.drop(self.df.columns[0:4], axis=1, inplace=True)
        self.df.drop(self.df.columns[:344], axis=1, inplace=True)

    def delete_next_year(self):
        self.df.drop(self.df.columns[0:4], axis=1, inplace=True)
        self.df.drop(self.df.columns[345:], axis=1, inplace=True)

    def filter(self):
        self.df = self.df.loc[self.df['Country/Region'] == 'Romania']

    def difference(self):
        self.df = self.df.diff(axis=1)

    def plot(self, x, y):
        self.df = pd.DataFrame({x: self.df.columns[0:], 
                                y: self.df.iloc[0]})
        self.df = self.df[::10]
        plt.style.use('seaborn-white')
        plt.plot(self.df[self.x], self.df[self.y], marker='o')
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlabel(self.x)
        plt.ylabel(self.y)
        plt.rcParams["figure.figsize"] = (30,10)
```

### Data Loading

Load the data:

```python
covid19_confirmed = pd.read_csv('COVID19_confirmed.csv')
covid19_deaths = pd.read_csv('COVID19_deaths.csv')
covid19_recovered = pd.read_csv('COVID19_recovered.csv')
```

### Data Processing for 2020

Filter, clean, and visualize the data for 2020:

```python
covid19_confirmed_selected_2020 = Covid(covid19_confirmed, "Date", "Cases confirmed")
covid19_confirmed_selected_2020.filter()
covid19_confirmed_selected_2020.delete_next_year()
covid19_confirmed_selected_2020.difference()

# Repeat for deaths and recoveries
```

Summarize data by month:

```python
january_2020_confirmed = covid19_confirmed_selected_2020.df.iloc[:,0:10].sum(axis=1)
# Repeat for other months and for deaths and recoveries
```

Plot the data:

```python
covid19_confirmed_selected_2020.plot("Date", "Cases confirmed")
plt.title('Confirmed cases of COVID19 in Romania from 2020 per days')
```

### Data Processing for 2021

Filter, clean, and visualize the data for 2021:

```python
covid19_confirmed_selected_2021 = Covid(covid19_confirmed, "Date", "Cases confirmed")
covid19_confirmed_selected_2021.filter()
covid19_confirmed_selected_2021.delete_extra_rows()
covid19_confirmed_selected_2021.difference()

# Repeat for deaths and recoveries
```

Summarize data by month:

```python
january_2021_confirmed = covid19_confirmed_selected_2021.df.iloc[:,1:32].sum(axis=1)
# Repeat for other months and for deaths and recoveries
```

Plot the data:

```python
covid19_confirmed_selected_2021.plot("Date", "Cases confirmed")
plt.title('Confirmed cases of COVID19 in Romania from 2021 per days')
```

## Results

The project provides detailed visualizations of the COVID-19 situation in Romania, highlighting the confirmed cases, deaths, and recoveries for the year 2020 and early 2021. The data is displayed on a daily basis and summarized monthly for better clarity.

## Notes

- The `SettingWithCopyWarning` occurs due to the operations performed on slices of the DataFrame. This can be ignored or handled with caution by creating copies explicitly.

## Contributing

Feel free to fork this project, create a feature branch, and submit a pull request for any improvements or bug fixes.

## License

This project is licensed under the MIT License.

*All data used in creating the plots is acquired from 'COVID-19 Data Repository by the Center for Systems Science and Engineering (CSSE) at Johns Hopkins University' GitGub